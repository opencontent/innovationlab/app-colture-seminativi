import { Component, OnInit } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';
import { LoadingController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'app-select-municipality',
  templateUrl: './select-municipality.page.html',
  styleUrls: ['./select-municipality.page.scss'],
})
export class SelectMunicipalityPage implements OnInit {
  comuni: any[];
  loading = true;
  error: any;
  selectedMunicipality: any;

  constructor(
    private apollo: Apollo,
    public modalCtrl: ModalController,
    public loadingController: LoadingController,
    public router: Router,
    private storage: StorageService
  ) {}

  ngOnInit() {
    this.loadingController.create({ keyboardClose: true }).then((loadingEl) => {
      loadingEl.present().then(() => {
        this.apollo
          .watchQuery({
            query: gql`
              query getComuni {
                municipalities {
                  name
                  id
                }
              }
            `,
          })
          .valueChanges.subscribe((result: any) => {
            this.comuni = result?.data?.municipalities;
            this.error = result.error;
            this.loading = false;
            loadingEl.dismiss();
          });
      });
    });
  }

  ionViewWillEnter() {
    this.storage.getMunicipality().then((res) => {
      this.selectedMunicipality = JSON.parse(res);
    });
  }

  async selectedHandle(data) {
    await this.storage
      .set(
        'municipality',
        JSON.stringify({
          id: data.id,
          name: data.name,
        })
      )
      .then(() => {
        console.log('setted');
        this.close(data);
      });
  }

  close(data?: any) {
    this.modalCtrl.dismiss(data);
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 2000,
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }
}
