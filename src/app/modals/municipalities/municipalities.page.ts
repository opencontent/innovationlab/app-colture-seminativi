import { Component, Input, OnInit } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'app-municipalities',
  templateUrl: './municipalities.page.html',
  styleUrls: ['./municipalities.page.scss'],
})
export class MunicipalitiesPage implements OnInit {
  @Input() municipality: any[];

  comuni: any[];
  loading = true;
  error: any;
  slideOpts = {
    initialSlide: 0,
    speed: 500
  };

  constructor(
    public modalCtrl: ModalController,
    public loadingController: LoadingController,
    public router: Router,
    private storage: StorageService
  ) {}

  ngOnInit() {
    this.loadingController.create({ keyboardClose: true }).then((loadingEl) => {
      loadingEl.present().then(() => {
        this.comuni = this.municipality;
        this.loading = false;
        loadingEl.dismiss();
      });
    });
  }

  ionViewWillEnter() {}

  async selectedHandle(data) {
    await this.storage
      .set(
        'municipality',
        JSON.stringify({
          id: data.id,
          comune: data.comune,
          nomecomune: data.nomecomune
        })
      )
      .then(() => {
        console.log('setted');
        this.close(data);
      });
  }

  close(data?: any) {
    this.modalCtrl.dismiss(data);
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 2000,
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }
}
