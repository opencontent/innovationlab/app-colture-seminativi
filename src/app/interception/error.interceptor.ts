import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';
import { StorageService } from '../services/storage.service';
import { Router } from '@angular/router';

export class ErrorInterceptor implements HttpInterceptor {
  constructor(public authService: AuthService, public storageService : StorageService, public router: Router){
  }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        let errorMsg = '';
        if (error.error instanceof ErrorEvent) {
          console.log('this is client side error');
          errorMsg = `Error: ${error.error.message}`;
        } else {
          console.log('this is server side error');
          errorMsg = `Error Code: ${error.status},  Message: ${error.message}`;
        }
        if(error.status === 401 || error.status === 500){
           this.storageService.removeToken().then(r => {
             console.log('removed token')
             this.router.navigate(['login']);
           })
        }
        console.log(errorMsg);
        return throwError(errorMsg);
      })
    );
  }
}
